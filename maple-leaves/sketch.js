let cycles = 12;

function setup() {
  createCanvas(800, 800, WEBGL);
  //   background(200);
  leaf = loadImage("leaf-yellow small.png");
  camera(0, 0, 1500, 0, 0, 0, 0, 1, 0);
}

function draw() {
  background(40);

  for (i = 0; i < cycles; i++) {
    rotate((360 / cycles) * frameCount * 0.0001);

    push();
    translate(0, -300, i * 5);
    texture(leaf);
    plane(400, 400);
    pop();
  }
}
