var asterisk;
var seq;
var wid = 900;
var hei = 720;
var phonograph;
var laser;
var frame;
var soundbtn;
var soundon;
var soundoff;
var c;
var cd;
var windowWidth;
var windowHeight;

function preload() {

    getAudioContext().suspend();

    seq = createSprite();
    s1 = loadImage('assets/stripey/stripey00.jpg');
    s2 = loadImage('assets/stripey/stripey01.jpg');
    s3 = loadImage('assets/stripey/stripey02.jpg');
    s4 = loadImage('assets/stripey/stripey03.jpg');
    s5 = loadImage('assets/stripey/stripey04.jpg');
    s6 = loadImage('assets/stripey/stripey05.jpg');
    s7 = loadImage('assets/stripey/stripey06.jpg');
    s8 = loadImage('assets/stripey/stripey07.jpg');
    s9 = loadImage('assets/stripey/stripey08.jpg');
    s10 = loadImage('assets/stripey/stripey09.jpg');
    s11 = loadImage('assets/stripey/stripey10.jpg');
    s12 = loadImage('assets/stripey/stripey11.jpg');
    s13 = loadImage('assets/stripey/stripey12.jpg');
    s14 = loadImage('assets/stripey/stripey13.jpg');
    s15 = loadImage('assets/stripey/stripey14.jpg');
    s16 = loadImage('assets/stripey/stripey15.jpg');
    s17 = loadImage('assets/stripey/stripey16.jpg');
    s18 = loadImage('assets/stripey/stripey17.jpg');


    phonograph = loadSound('assets/phonograph6.mp3');
    laser = loadSound('assets/laser.mp3');

    soundon = loadImage('assets/soundon.svg');
    soundoff = loadImage('assets/soundoff.svg');

}

function touchStarted() {
    if (getAudioContext().state !== 'running') {
      getAudioContext().resume();
    soundbtn.state = 0;
    soundbtn.icon = soundoff;
    };
    soundbtn.clicked();
}

function setup() {

        // pixelDensity(1);
        // cd = document.getElementById("defaultCanvas0");
        
        createCanvas(windowWidth, windowHeight);

        soundbtn = new Soundbtn();
        soundbtn.placebtn(30, windowHeight-30);

        phonograph.loop();
        laser.setVolume(0);
        laser.loop();

        p1 = loadImage('assets/breakout/breakout00.jpg');
        p2 = loadImage('assets/breakout/breakout01.jpg');
        p3 = loadImage('assets/breakout/breakout02.jpg');
        p4 = loadImage('assets/breakout/breakout03.jpg');
        p5 = loadImage('assets/breakout/breakout04.jpg');
        p6 = loadImage('assets/breakout/breakout05.jpg');
        p7 = loadImage('assets/breakout/breakout06.jpg');
        p8 = loadImage('assets/breakout/breakout07.jpg');
        p9 = loadImage('assets/breakout/breakout08.jpg');
        p10 = loadImage('assets/breakout/breakout09.jpg');
        p11 = loadImage('assets/breakout/breakout10.jpg');
        p12 = loadImage('assets/breakout/breakout11.jpg');
        p13 = loadImage('assets/breakout/breakout12.jpg');
        p14 = loadImage('assets/breakout/breakout13.jpg');
        p15 = loadImage('assets/breakout/breakout14.jpg');
        p16 = loadImage('assets/breakout/breakout15.jpg');
        p17 = loadImage('assets/breakout/breakout16.jpg');
        p18 = loadImage('assets/breakout/breakout17.jpg');

        seq.addAnimation('stripey', s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18); 
        seq.addAnimation('breakout', p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18); 


        placeSprite();
        scaleSprite();

        seq.onMousePressed = function() {
            this.changeAnimation('breakout');
            phonograph.setVolume(0);
            laser.setVolume(0.4);
        };

        seq.onMouseReleased = function() {
            this.changeAnimation('stripey');
            phonograph.setVolume(1);
            laser.setVolume(0);
        };
}

function placeSprite() {
    seq.position.x = windowWidth/2;
    seq.position.y = windowHeight/2;
}

function scaleSprite() {
    if (windowWidth > windowHeight) {
        seq.scale = windowHeight/hei;
    }
    else {
        seq.scale = windowWidth/wid;
    }
}


function windowResized() { 

    resizeCanvas(windowWidth, windowHeight);
    scaleSprite();
    soundbtn.placebtn(30, windowHeight - 30);
    // console.log(soundbtn.x, soundbtn.y);


}

function draw() {

    background(14, 14, 14);
    drawSprite(seq);
    camera.off();
    soundbtn.display();  

    // soundbtn.placebtn(30, windowHeight - 30);
    // image(soundbtn.icon, 30, windowHeight - 30);
    // console.log(soundbtn.x, soundbtn.y);

}



class Soundbtn {
    constructor() {
      this.state = 0;  
      this.icon = soundon;
    }
  
    clicked() {
        let d = dist(mouseX, mouseY, this.x, this.y);
        if (d<30) {
            this.changeState();
        };
    }

    changeState() {
        if (this.state == 0) {
            this.icon = soundon;
            this.state = 1;
            masterVolume(0);
        } else {
            this.icon = soundoff;
            this.state = 0;
            masterVolume(1);
        };
    }

    placebtn(xx, yy) {
        this.x = xx;
        this.y = yy; 
    }

    display() {
        image(this.icon, this.x, this.y);
    }
  }





//     // cd = document.getElementById("defaultCanvas0");
//     // c.position(windowWidth/2, windowHeight/2); 
//     // console.log(windowWidth, windowHeight);
//     // console.log(seq.position.x, seq.position.y);
//     console.log(c.width, c.height);
//     // console.log(cd.width, cd.height);

