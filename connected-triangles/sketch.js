const margin = 100;
const neighRadius = 150;
cycles = 200;
let alpha = 0.1;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(112, 193, 179);

  N = randomN();

  for (i = 0; i < cycles; i++) {
    BC = neighbours(N, alpha);
    AX = N[0];
    AY = N[1];
    BX = BC[0];
    BY = BC[1];
    CX = BC[2];
    CY = BC[3];

    alpha = map(i, 0, cycles, 0.1, 1);

    noStroke();
    fill(`rgba(27,90,117, ${alpha})`);
    triangle(AX, AY, BX, BY, CX, CY);

    N1 = randomN();
    A1X = N1[0];
    A1Y = N1[1];
    if (i < cycles - 1) {
      stroke(`rgba(10,35,66,${alpha * 0.5})`);
      line(CX, CY, A1X, A1Y);
    }
    N = N1;
  }
}

function draw() {}

function randomN() {
  x = random(margin, width - margin);
  y = random(margin, height - margin);
  N = [x, y];
  return N;
}

function neighbours(N, alpha) {
  BX = N[0] + random(-neighRadius * alpha, neighRadius * alpha);
  BY = N[1] + random(-neighRadius * alpha, neighRadius * alpha);
  CX = N[0] + random(-neighRadius * alpha, neighRadius * alpha);
  CY = N[1] + random(-neighRadius * alpha, neighRadius * alpha);
  BC = [BX, BY, CX, CY];
  return BC;
}
