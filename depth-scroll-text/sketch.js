let myFont;
let pg;
let depth = 300;

function preload() {
  myFont = loadFont("Arial Black.ttf");
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  pg = createGraphics(400, 400);
}

function draw() {
  background(220);

  // pg.background(0, 0, 0, 0);
  // pg.fill("#ED225D");
  // pg.textFont(myFont);
  // pg.textSize(36);
  // pg.textAlign(CENTER);
  // pg.translate(pg.width / 2, pg.height / 2);
  // pg.text("TEXT 1", 100, 50);
  // texture(pg);
  // translate(0, 0, depth);
  // plane(400, 400);

  fill("#ED225D");
  textFont(myFont);
  textSize(36);
  textAlign(CENTER);
  translate(0, 0, depth / 2);
  text("HELLO", 0, 0);

  translate(0, 0, depth / 2 - 1000);
  text("WORLD", 0, 0);
}

function mouseWheel(event) {
  depth += event.delta;
  print(depth);
  return false;
}
