
    //get random split amount per axis
    let n1X;
    let n1Y;

    //get random split amount for secondary level split
    let n2X;
    let n2Y;
    let n2;

    // introduce randomness to average split
    let jitter1;
    let jitter2;

    //colors
    let mred;
    let mblue;
    let myellow;
    let mwhite;
    let mblack;
    let mcolors = [];
    let mstroke;

    let pointsX = [];
    let points2X = [];
    let pointsY = [];
    let points2Y = [];
    let shortPointsX = [];

    let paper;

    let timer;
    let time = 6;

    function preload(){
      paper = loadImage("grain_paper_3.jpg");  
    }

function setup() {
    var canvas = createCanvas(windowWidth, windowHeight);
    canvas.parent('sketch-holder');
    canvas.class('canvas');
    // paper = loadImage('grain_paper_1.jpg');
   

    //colors
     mred = color(201, 77, 10);
     mblue = color(19, 79, 154);
     myellow = color(254, 235, 136);
     mwhite = color(252, 252, 252);
     mblack = color(28,28,28);
     mcolors = [   mwhite, mwhite,mwhite,mwhite,mwhite,mwhite,mwhite, mblue, mwhite, mred,mwhite,mwhite,myellow, mwhite,mwhite,mwhite,mwhite];
     mstroke = 5;
    
    reredraw();
    
    // button = createButton('Mondrian!');
    // button.parent('button-holder');
    // // button.position(windowWidth/2, 20);
    canvas.mousePressed(reredraw);

}





function reredraw() {
  //FIRST PASS 
   //get random split amount per axis

    clear();
    n1X = floor(random(2,8));
    n1Y = floor(random(2,8));
    timer = time;


    jitter1 = width/n1X/3;
    jitter2 = 0;
    //get random split amount for secondary level split

    n2 = int(random(0,4));
    let xx1;
    let xx2;
    shortPointsX = [];

    //get split for each axis
    pointsX = msplit(n1X, 0, width, jitter1);
    pointsY = msplit(n1Y, 0, height, jitter1);

    //draw rectagles 
    populate(pointsX, pointsY);
    
  //SECOND PASS

    for (let n = 0; n<n2; n++) {
      n2X = int(random(1,3));
      n2Y = int(random(1,3));
      secondPointsX = secondSplit(pointsX, n2X);
      secondPointsY = secondSplit(pointsY, n2Y);
      populate(secondPointsX, secondPointsY);
    }
    
    blend(paper, 0, 0, paper.width, paper.height, 0, 0, width, height, MULTIPLY);
    // rect(0,0,width,height);


}

function draw() {
  // image(paper, 50, 50); 

  // blend(paper, 50, 50, 900, 600, 50, 50, 900, 600, MULTIPLY);
  if (frameCount % 60 == 0 && timer > 0) { // if the frameCount is divisible by 60, then a second has passed. it will stop at 0
    timer --;
  }
  if (timer == 0) {
    reredraw();
    
  }
  // console.log(timer);


}

function secondSplit(inPoints,num) {    
  let shortinPoints = [];    
    //making short arrays for spotting random points from previous split, excluding last point
  for (i=0; i<inPoints.length-1; i++) {
    shortinPoints[i] = inPoints[i];
  };  

  //finding random point and point next to it
    xx1 = random(shortinPoints);
    let index = inPoints.findIndex(inPoints => inPoints==xx1);
    xx2 = inPoints[index+1];
    outPoints = msplit(num, xx1, xx2, 0);
    // print(outPoints);
    // print(xx1, index);
    // print(xx2, index+1);

    // print(shortinPoints);
    // print(pointsX);
    return outPoints;
}

function populate(pointsX, pointsY){
  for (i=0; i<(pointsX.length - 1); i++){
    for (j=0; j<(pointsY.length - 1); j++) {
  
      fill(random(mcolors));
      stroke(mblack);
      strokeWeight(mstroke);
  
      rect(pointsX[i], pointsY[j], pointsX[i+1]-pointsX[i], pointsY[j+1]-pointsY[j]) 
    }
  }

  noFill();
  // stroke(color(182, 182, 182));
  stroke(color(242, 242, 242));
  strokeWeight(mstroke*6);
 
}



function msplit(num, min, max, jitter) {
  let points = [];
  points[0] = min;
  points[num] = max;
  let total = abs(max - min);
  let avg = total/num;
  for(i=1; i<num; i++) {
    do {points[i]=int(min+avg*i+random(-jitter, jitter));} 
    while (points[i]<min || points[i]>max);
  } 

  return points.sort(function(a,b){ 
    return a - b
  });
}

 


function windowResized() { 
  resizeCanvas(windowWidth, windowHeight); 
  reredraw(); 
}


   